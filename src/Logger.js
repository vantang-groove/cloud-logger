import Winston, { Logger } from 'winston';
import { LoggingWinston as StackDriver } from '@google-cloud/logging-winston';
import AwsCloudWatch from 'winston-aws-cloudwatch';

export default class CustomLogger {
  constructor(options) {
    this.provider = null;
    this.logger = null;
    this.config = null;

    if (!this.checkProperty(options, 'provider')) {
      console.log('Missing logging provider');
      return null;
    }

    this.provider = options.provider;

    let transports = [];
    switch (this.provider) {
      case 'local': {
        transports = [new (Winston.transports.File)({
          ...options.config
        })];
        break;
      }
      case 'google': {
        transports = [new StackDriver({
          ...options.config
        })];
        break;
      }
      case 'aws': {
        transports = [new AwsCloudWatch({
          ...options.config
        })];
        break;
      }
      default: {
        console.log(`Error provider ${this.provider} is not supported for now`);
        break;
      }
    }

    if (transports.length !== 0) {
      this.logger = new Logger({
        level: 'info',
        transports,
        exitOnError: false,
      });
    }
  }

  checkProperty = (object, prop) => Object.prototype.hasOwnProperty.call(object, prop);
  checkInstance = () => this.provider && this.logger;

  info = message => new Promise((resolve, reject) => {
    if (!this.checkInstance()) {
      console.log('Logger is null');
      return reject();
    }

    if (!message) {
      console.log('Missing message to log');
      return reject();
    }

    return this.logger.info(message, (error) => {
      if (error) {
        return reject(error);
      }

      return resolve();
    });
  });

  error = message => new Promise((resolve, reject) => {
    if (!this.checkInstance()) {
      console.log('Logger is null');
      return reject();
    }

    if (!message) {
      console.log('Missing message to log');
      return reject();
    }

    return this.logger.error(message, (error) => {
      if (error) {
        return reject(error);
      }

      return resolve();
    });
  });

  warn = message => new Promise((resolve, reject) => {
    if (!this.checkInstance()) {
      console.log('Logger is null');
      return reject();
    }

    if (!message) {
      console.log('Missing message to log');
      return reject();
    }

    return this.logger.warn(message, (error) => {
      if (error) {
        return reject(error);
      }

      return resolve();
    });
  });
}
