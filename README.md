# enablo-cloud-logger
A wrapper library using winston logging for Google StackDriver and Aws CloudWatchLogs. This also supports logging locally for debugging.

## Installing

### Via npm

```
npm install --save @enablo/cloud-logger
```

### Via yarn

```
yarn add @enablo/cloud-logger
```

## Set up provider SDK/Cli
Follow these tutorials below to set up services corresponding to your provider
* Install and set up  [Google Cloud SDK](https://cloud.google.com/deployment-manager/docs/step-by-step-guide/installation-and-setup)
* Install and set up  [AWS CLI](https://docs.aws.amazon.com/lambda/latest/dg/setup-awscli.html)

## Usages
### Constructor
Object constructor must be declared before use, you should specify the provider name and config properties as an object passing to constructor.
#### config object properties
* provider (string): name of the service provider, this muse be one of 'google', 'aws' or 'local'.
* config (object): any config properties supported by the corresponding to provider.
#### Google StackDriver

```
import Logger from '@enablo/cloud-logger';
const logger = new Logger({
    provider: 'google',
    config: {
        projectId: 'your-project-id,
        keyFilename: 'your-key-file-name'
        logName: 'your-log-name'
    }
});
```
#### Config properties for google provider

|  No. | Property | type | Required  | Meaning | Default |
|:---:|:---|:---|:---:|:---|:---|
| 1 | projectId | string |  Yes | project id  |  | 
| 2 | keyFilename | string | No  | path to to key.json |
| 3 | logName | string |No | log name | winston_log | 

#### AWS CloudWatchLogs
```
import Logger from '@enablo/cloud-logger';
const logger = new Logger({
    provider: 'aws',
    config: {
        logGroupName: 'your-group-log-name',
        logStreamName: 'your-group-stream-name',
        awsConfig: {
            region: 'your-region',
            ...
        }
    }
});
```

#### Config properties for aws provider

|  No. | Property | type | Required  | Meaning | default | Note|
|:---:|:---|:---|:---:|:---|:---:|:---|
| 1 | logGroupName | string |  Yes | log group name  |  | 
| 2 | logStreamName | string | Yes  | log stream name | |
| 3 | createLogGroup | boolean | No  | will create log group if not exists ? | false |
| 4 | createLogStream | boolean | No  | will create log stream if not exists ? | false |
| 5 | submissionInterval | number | No | seconds of submission interval | |
| 6 | submissionRetryCount | number | No | seconds of submission retry count | |
| 7 | batchSize | number | No | number of batch size | |
| 3 | awsConfig | object | Yes | config for aws object | | [see here](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property)|

#### Local
```
import Logger from '@enablo/cloud-logger';
const logger = new Logger({
    provider: 'local',
    config: {
        name: 'your-log-name',
        filename: 'path-to-log.log'
    }
});
```

## Methods
There are threee public methods for logging which have the same arguments. You can pass a string, a number or an object as an param.
### info
```
import Logger from '@enablo/cloud-logger';
const logger = new Logger({});

const message = {
      type: 'type',
      payload: {
        numberList: [1, 2, 3, 4, 5, 6, 7, 8],
        someStr: 'content'
      },
};

logger.info(message).then((data) => {
    console.log('info message logged', data);
}).catch((error) => { console.log('error', error) });
```

### error
```
import Logger from '@enablo/cloud-logger';
const logger = new Logger({});

const message = 'my-string';

logger.error(message).then((data) => {
    console.log('error message logged', data);
}).catch((error) => { console.log('error', error) });
```

### warn
```
import Logger from '@enablo/cloud-logger';
const logger = new Logger({});

const message = 2018;

logger.warn(message).then((data) => {
    console.log('warning message logged', data);
}).catch((error) => { console.log('error', error) });
```

## Example
You may want to run an example to test this library. Pick one sample env file in **Example** folder corresponding to the service you want to test, change its name to **.env** then fill in your project credentials.

For example, if you want to test this library using **Google StackDriver**. In Example folder, change the file whose name is **sample.google.env** to **.env**. You must fill in your projectId before following these steps to run example:

```
cd Example
yarn or npm install
yarn start or npm start
```

## Contributing

## Versioning

## Authors

## License

## Acknowledgments