import Logger from '../../src/Logger';
import dotenv from 'dotenv/config';

const GROUNAME = process.env.LOG_GROUP_NAME;
const STREAMNAME = process.env.LOG_STREAM_NAME;
const REGION = process.env.REGION;

export default class Example {
  constructor() {
    this.logger = new Logger({
      provider: 'aws',
      config: {
        logGroupName: GROUNAME,
        logStreamName: STREAMNAME,
        createLogGroup: true,
        createLogStream: true,
        awsConfig: {
          region: REGION
        }
      }
    });
  }

  run = async () => {
    // publicMessage(messageObject)
    const message = {
      type: 'type',
      payload: {
        numberList: [1, 2, 3, 4, 5, 6, 7, 8],
        someStr: 'content'
      },
      attributes: {
        key1: 'key1',
        key2: 'key2',
        key3: 'key3'
      }
    }

    this.logger.info(message).then(() => {
      console.log('logged successfully')
    }).catch((error) => {
      console.log('Error', error);
    });
  }
}