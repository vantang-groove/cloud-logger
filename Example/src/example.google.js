import Logger from '@enablo/cloud-logger';
import dotenv from 'dotenv/config';

const PROJECTID = process.env.PROJECTID;
const TOPIC = process.env.TOPICNAME;

export default class Example {
  constructor() {
    this.logger = new Logger({
      provider: 'google',
      logName: 'test-log-name',
      config: {
        projectId: PROJECTID
      }
    });
  }

  run = async () => {
    // publicMessage(messageObject)
    const message = {
      type: 'type',
      payload: {
        numberList: [1, 2, 3, 4, 5, 6, 7, 8],
        someStr: 'content'
      },
      attributes: {
        key1: 'key1',
        key2: 'key2',
        key3: 'key3'
      }
    }

    this.logger.info(message).then(() => {
      console.log('logged successfully')
    }).catch((error) => {
      console.log('Error', error);
    });
  }
}