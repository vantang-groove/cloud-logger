import express from 'express';
import GooleStackDriver from './src/example.google';
import AwsCloudWatch from './src/example.aws';
import localLog from './src/example.local';
import dotenv from 'dotenv/config';

const PROVIDER = process.env.PROVIDER;

console.log('provider', PROVIDER);

const app = express();
const port = 3000;

app.listen(port, async (err) => {
  if (err) {
    return console.error(err);
  }

  var client;
  if (PROVIDER === 'google') {
    client = new GooleStackDriver();
  }

  if (PROVIDER === 'aws') {
    client = new AwsCloudWatch();
  }

  if (PROVIDER === 'local') {
    client = new localLog();
  }

  client.run();
});
