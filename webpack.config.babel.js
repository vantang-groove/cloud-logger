import path from 'path';
import nodeExternals from 'webpack-node-externals';

const config = {
  target: 'node',
  entry: {
    app: './ES6.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
    library: 'EnabloCloudLogger',
    libraryTarget: 'commonjs2',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules)/,
      loader: 'babel-loader',
      query: {
        presets: [
          'es2016'
        ],
      }
    }],
  },
  externals: [nodeExternals()],
};

export default config;
